var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	constants = require('../constants');

var loginSchema = new Schema({
	userName : {type: String},
    userPass : {type: String}

}, { collection: constants.Login, autoIndex: true, timestamps: true });
 
mongoose.model(constants.Login, loginSchema);
