var express = require('express');
var router = express.Router();
var controller = require('./controller/myController');
var middleware = require('./middleware');

router.post('/get-token', controller.getAuthToken);
router.post('/sample-url', middleware.validateToken, controller.testFunction);

module.exports = router; 