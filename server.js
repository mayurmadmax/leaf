var express = require('express'),
    logger = require('morgan'),
    bodyParser = require('body-parser'),
    cluster = require('cluster'),
    port = 4321,
    numCPUs;


var app = module.exports = express();

numCPUs = require('os').cpus().length;
process.on('uncaughtException', function (err) {
    console.log(err.stack);
});

if (cluster.isMaster) {
    for (var i = 0; i < numCPUs; i++) {
        cluster.fork();
    }
    cluster.on('exit', function (worker) {
        console.log(' Forked myself!!!!');
        console.log('Worker ' + worker.id + ' died..'); 
    });
} else {
    require('./connections/con-mongo.js');
    require('./model/loginModel.js');
    app.use(logger('dev'));
    app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));
    app.use('/', require('./routes'));
    app.set('port', port);
    // Error callback
    app.use(function (err, req, res, next) {
        console.error(err.stack);
        res.status(500).send('Something broke!');
    });
    
    var server = app.listen(app.get('port'), function () {
        console.log('Express server listening on port ' + server.address().port);
    });
}