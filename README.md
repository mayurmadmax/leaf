Problem Statement :

You have to implement user authentication using NodeJs and MongoDB. 
You have to consider the following in your solution-
 
1. Use RESTfull architecture.
2. Use JSON.
3. Use JWT token for verification.
4. You are free to use any web framework.

*************************************************************
*************************************************************
*************************************************************

Solution :

Steps to proceed:
*************************************************************
1) Please install npm install

2) if you have pm2 installed then start server.js using the same or proceed with 'node server.js'

3) You need to have mongo up and running with lead DB and also, login collection must ne there to proceed.

4) Else you can comment line: 26-29 and uncomment line: 31 of file controller/myController.js to bypass mongo work and simulate the login creds.

5) The application is running at 4321 port.

6) We have 2 apis:

a) POST : /get-token - 
	Sample request:
	{
		"uName" : "Mayur",
		"uPass" : "Rahul"
	}
	Sample response:
	{
		"response": "success",
		"token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1TmFtZSI6Ik1heXVyIiwidVBhc3MiOiJSYWh1bCIsImdlbmVyYXRlZEF0IjoxNDkyMDE1ODgyNzgxLCJjdXN0b21JbmZvIjoiZ2VuZXJhdGVkIGJ5IG15IHNlcnZlciJ9.smC-5V4bW6hz-Z1xqubFFFniDxUMggzwwkRvrF_KOzY"
	}

b) POST : /sample-url -
	Sample request:
	Provide header x-access-token : with the token provided from get-token to proceed.


*************************************************************
*************************************************************


POSTMAN LINK : https://www.getpostman.com/collections/bc2afcf17b6841bce161


*************************************************************
*************************************************************
