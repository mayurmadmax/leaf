var constants = require('./constants.js');
var jwt = require('jwt-simple');
var middleware = {
	validateToken : function(req, res, next) {
		var errorResponse = {
				response : 'failure',
				error : 'invalid token supplied'
			},
			getToken = req && req.headers && req.headers['x-access-token'];
		if(!getToken) {
			res.status(400);
			return res.json({
				response : 'failure',
				error : 'missing params'
			})
		}
		try {
			var decoded = jwt.decode(getToken, constants.secret, true);
			if(!decoded.uName || !decoded.uPass || !decoded.generatedAt || !decoded.customInfo) {
				res.status(400);
				return res.json(errorResponse);
			}
			next();
		}
		catch(e) {
			console.log("ErrorException:",e);
			res.status(400);
			return res.json(errorResponse);
		}
	}
};

module.exports = middleware;