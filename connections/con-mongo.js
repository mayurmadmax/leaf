var mongoose = require('mongoose'),
  uri = "mongodb://127.0.0.1:27017/leaf",
  options = {
    server: {
        socketOptions: {
            keepAlive: 30000
        },
        poolSize: 500
    },
    user: '',
    pass: '',
    db: {
        native_parser: true
    }
  };


var connectMongoose = function () {
    mongoose.connect(uri, options);
};
connectMongoose();

// Error handler
mongoose.connection.on('error', function (err) {
    console.log(err);
});

mongoose.connection.on('open', function () {
    console.log("connection open");
});

// Reconnect when closed
mongoose.connection.on('disconnected', function () {
    setTimeout(function () {
        connectMongoose();
    }, 1000);
});